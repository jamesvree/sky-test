# SkyTest

This project uses Terraform to create a VPC in AWS eu-west-2 region (London) with an EC2 Instance for hosting a web service. The web service of choice is nginx deployed within a custom Docker image which exposes port 8080 on localhost.

The host uses nginx to reverse proxy from port 443 to port 8080 in docker.

## Terraform

To deploy the environment, change directory to terraform and run
` terraform init `
` terraform plan `
` terraform apply `

To destroy the environment run

` terraform destroy `

TODO: Create Ansible task to deploy using terraform configuration